
# import argparse
import logging
import ffmpeg 
import os
import sys
from tqdm import tqdm


logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(name)s %(funcName)s(): %(message)s")
logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

'''
not work
'''
# in_file = ffmpeg.input(input("Input your MP4 which is convert (<filename>.mp4):"))
# out_file = ffmpeg.input(input("Input your the filename of output MP3 (<filename>.mp3): "))
# (
#     ffmpeg
#     # .audio.filter(acodec="libmp3lame",audio_bitrate=320000,)
#     .output(input("Input your the filename of output MP3 (<filename>.mp3): "), **{'acodec': "libmp3lame",
#     "b:a": 320000})
#     .run_async(pipe_stdin=True)
# )
# audio = in_file.filter(acodec="libmp3lame", audio_bitrate=320000)
# out_file = ffmpeg.output(audio, input("output filename (.mp3)"))

'''
Work
'''
# input = ffmpeg.input('in.mp4')
# audio = input.audio.filter("aecho", 0.8, 0.9, 1000, 0.3)
# video = input.video.hflip()
# out = ffmpeg.output(audio, video, 'out.mp4')


def show_progress(total_duration):
    """Create a unix-domain socket to watch progress and render tqdm
    progress bar."""
    with tqdm(total=round(total_duration, 2)) as bar:
        def handler(key, value):
            if key == 'out_time_ms':
                time = round(float(value) / 1000000., 2)
                bar.update(time - bar.n)
            elif key == 'progress' and value == 'end':
                bar.update(bar.total - bar.n)


# if __name__ == "__main__":

# out = input("Input your the filename of output MP3 (<filename>.mp3): ")
in_file="111.mp4"
out="222.mp3"

# total_duration = float(ffmpeg.probe(args.in_filename)['format']['duration'])
total_duration = float(ffmpeg.probe(in_file)['format']['duration'])
logger.info(total_duration)

# with show_progress(total_duration):
sepia_values = [.393, .769, .189, 0, .349, .686, .168, 0, .272, .534, .131]
try:
    out, err = (ffmpeg
    .input(in_file)
    # .colorchannelmixer(*sepia_values)
    .output(os.path.splitext(in_file)[0]+".mp3", format='mp3', acodec='libmp3lame', ac=2, ar='48k', ab='320k')
    # .global_args('-progress', progress)
    .overwrite_output()
    .run(capture_stdout=True, capture_stderr=False)
    )
except ffmpeg.Error as e:
    logger.error(e.stderr, file=sys.stderr)
    sys.exit(1)
logger.info(out)
# out.run()
