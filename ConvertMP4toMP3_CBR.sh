#!bin/bash

# First parameter: <input MP4>
# Second parameter: <output MP3>

if [ "$#" -ne "2" ]; then
    echo "usage: $0 <input.mp4> <output.mp3>"
    exit 1
fi

input=$1
output=$2

#ffmpeg -i $input -vn \
#       -acodec libmp3lame -ac 2 -ab 320k -ar 48000 \
#        $output

ffmpeg -i $input -vn \
       -acodec libmp3lame -b:a 320k \
        $output
